import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Array;

public class Main {
    private static Font headFont = new Font(Font.FontFamily.TIMES_ROMAN, 50);
    private static String[] text = new String[10];

    private static PdfPTable bodyRender(){

        PdfPTable table = new PdfPTable(2);
        text[0] = "First name";
        text[1] = ("Dominik Irytowski");
        text[2] = ("Last name");
        text[3] = ("Irytowski");
        text[4] = ("Proffesion");
        text[5] = ("Student");
        text[6] = ("Education");
        text[7] = ("2018 - 2021 PWSZ Tarn�w");
        text[8] = ("Summary");
        text[9] = ("I am here to lear something!!!");

        for(int i = 0; i < 10; i++){
            PdfPCell cell = new PdfPCell(new Phrase(text[i]));
            cell.setPadding(10);
            table.addCell(cell);
        }

        return table;
    }
    private static Paragraph headRender(){
            Paragraph para = new Paragraph();
            para.add(new Paragraph("Resume", headFont));
            para.setAlignment(Element.ALIGN_CENTER);
        return para;
    }

    public static void main(String[] args) {
        try {
            Document document = new Document(PageSize.A4);
            PdfWriter.getInstance(document, new FileOutputStream("./resume.pdf"));
            document.open();
            document.add(headRender());
            document.add(Chunk.NEWLINE);
            document.add(Chunk.NEWLINE);
            document.add(bodyRender());
            document.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("PDF CREATED!");
    }
}
